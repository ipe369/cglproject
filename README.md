# To build
## Dependencies

- GLFW

```
    # Use this to pull in git submodule dependencies like GLFW
    git submodule update --init --recursive
```

## Platform specifics

### On windows
When building using visual studio, make sure the visual C bin folder is on your
path, then run vcvarsall before doing anything. Afterwards, run the following
commands to build:

```
$ mkdir build
$ cd build
$ cmake ..
$ msbuild ALL_BUILD.vcxproj
$ cd ..
```