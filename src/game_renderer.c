#include "game_renderer.h"
#include "game_state.h"

void game_renderer_init(Game_Renderer* renderer,
                        const int screen_w,
                        const int screen_h) {
  renderer->screen_w = screen_w;
  renderer->screen_h = screen_h;
}


void game_renderer_render(const Game_Renderer* renderer,
                          const Game_State* state) {
}
