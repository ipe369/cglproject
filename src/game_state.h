/**
   @file game_state.h
*/

#ifndef GAME_STATE_H
#define GAME_STATE_H

/* Fwd decl */
struct Chunk;

/**
   @brief Holds state related to the game. 
   For example, a list of chunks currently loaded in memory.
*/
struct Game_State {
  /** Number of chunks in chunk_list. */
  int num_chunks;
  /** 
      Pointer to a list of chunks. Potentially a linked list, depends
      how fast chunks get loaded and unloaded. 
  */
  struct Chunk* chunk_list;
};
typedef struct Game_State Game_State;

/**
   @brief Initialises a Game_State struct.
   @param[out] state A pointer to the state to initialise.
   @param[in] num_chunks The number of chunks to initially allocate.
   @memberof Game_State
   @public
*/
void game_state_init(Game_State* state, const int num_chunks);

/**
   @brief Cleans up resources owned by this Game_State.
   @param[in,out] state The Game_State to destroy.
   @memberof Game_State
   @public
*/
void game_state_destroy(Game_State* state);

#endif 
