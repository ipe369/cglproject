#include <stdlib.h>
#include "game_state.h"
#include "chunk.h"

void game_state_init(Game_State* state, const int num_chunks) {
  state->num_chunks = num_chunks;
  state->chunk_list = (Chunk*) malloc(sizeof(Chunk));
}

void game_state_destroy(Game_State* state) {
  if (state->chunk_list != NULL) {
    free(state->chunk_list);
  }
}

