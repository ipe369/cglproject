/**
   @file vertex.h
 */

#ifndef VERTEX_HEADER
#define VERTEX_HEADER

#include <glad/glad.h>

/**
   @brief The vertex format for colored voxels.
 */
struct Vertex_Voxel_Color {
  // START ivec4
  /** Position */
  union {
    struct {
      GLubyte x;
      GLubyte y;
      GLubyte z;
    };
    GLubyte[3] xyz;
  };
  
  /** Normal */
  GLubyte n;
  // END ivec4

  // START vec4
  /** Color */
  union {
    struct {
      GLubyte r;
      GLubyte g;
      GLubyte b;
      GLubyte a;
    };
    GLubyte[4] rgba;
  };
  // END vec4
};

/** Typedef for convenience */
typedef struct Vertex_Voxel_Color Vertex_Voxel_Color;

#endif
