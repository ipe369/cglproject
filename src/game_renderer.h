/**
   @file game_renderer.h
 */

#ifndef GAME_RENDERER_H
#define GAME_RENDERER_H
#include <GLFW/glfw3.h>

/* Fwd decl */
struct Game_State;

/**
   @brief Renderer used to render the game. 

   Holds 'properties' of rendering, things like screen bounds and shader
   program IDs.
*/
struct Game_Renderer {
  int screen_w, screen_h;
  int num_vba;
  GLuint* vba_list;
};

/** Typedef for convenience */
typedef struct Game_Renderer Game_Renderer;

/**
   @brief Initialises a game renderer
   @param[out] renderer The renderer to initialise
   @param[in] _screen_w The width of the renderer
   @param[in] _screen_h The height of the renderer
   @memberof Game_Renderer
   @public
*/
void game_renderer_init(Game_Renderer* renderer,
                        const int screen_w,
                        const int screen_h);

/**
   @brief Renders the game given a renderer and some game state.
   @param[in] renderer The renderer to use.
   @param[in] state The state to render.
   @memberof Game_Renderer
   @public
*/
void game_renderer_render(const Game_Renderer* renderer,
                          const struct Game_State* state);

#endif
