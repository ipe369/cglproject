/**
   @file main.c
   @brief Contains program entry point.
 */
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <stdio.h>
#include <stdlib.h>
#include "game_renderer.h"
#include "game_state.h"

/**
   @brief GLFW key callback function
 */
void glfw_key_callback(GLFWwindow* w,
                       int key,
                       int scancode,
                       int action,
                       int mods) {
  if (key == GLFW_KEY_ESCAPE) {
    glfwSetWindowShouldClose(w, 1);
  }
}

/**
   @brief Initialise GLFW and set up a window
 */
GLFWwindow* init_glfw_window() {
  GLFWwindow* w = 0;

  glfwInit();
  /* Set GLFW to try and use OpenGL 3.3 */
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  /* We need this! This will stop us using deprecated functionality, and the */
  /* mesa drivers on linux only support GL 3.3 core profile, not compatibility! */
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  /* Create window */
  w = glfwCreateWindow(800, 600, "Title_Here", NULL, NULL);
  glfwMakeContextCurrent(w);
  /* Set input callbacks */
  glfwSetKeyCallback(w, glfw_key_callback);

  /* Load OpenGL extensions with GLAD */
  if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
    printf("Can't load OpenGL extensions\n");
  }
  printf("OpenGL Ver - %d.%d\n", GLVersion.major, GLVersion.minor);

  return w;
}

/**
   @brief Program entry point
 */
int main(int argc, char** argv) {
  Game_Renderer* game_renderer =
    (Game_Renderer*) malloc(sizeof(Game_Renderer));
  game_renderer_init(game_renderer, 800, 600);
  Game_State* game_state =
    (Game_State*) malloc(sizeof(Game_State));
  game_state_init(game_state, 8);
  GLFWwindow* window = init_glfw_window(game_renderer->screen_w,
                                        game_renderer->screen_h);

  while (1) {
    game_renderer_render(game_renderer, game_state);
    glfwSwapBuffers(window);
    glfwPollEvents();
    if (glfwWindowShouldClose(window)) {
      break;
    }
  }

  return 0;
}

