#include <string.h>
#include "chunk.h"

void chunk_init(Chunk* chunk, int voxel_values) {
  memset(chunk->voxels, voxel_values, sizeof(chunk->voxels));
}

