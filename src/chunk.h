/**
   @file chunk.h
 */

#ifndef CHUNK_HEADER
#define CHUNK_HEADER
#include <stdint.h>

#define CHUNK_W 16
#define CHUNK_H 16
#define CHUNK_L 16
/* Not sure whether this actually performed the arithmetic or just */
/* substitutes text. Either way will be cleaned up by an optimising */
/* compiler. */
#define CHUNK_SIZE CHUNK_W * CHUNK_H * CHUNK_L

/**
   @brief Structure to hold a 'list' of blocks and their position
   relative to this chunk, to be loaded and unloaded as a group.
 */
struct Chunk {
  uint16_t voxels[CHUNK_SIZE];
};
typedef struct Chunk Chunk;

/**
   @brief Initialises a Chunk with the given values.
   @param[out] chunk The chunk to init.
   @param[in] voxel_values The value to set all the voxels of the chunk
   @memberof Chunk
   @public
 */
void chunk_init(Chunk* chunk, int voxel_values);

#endif
